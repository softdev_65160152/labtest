package com.mycompany.lab03;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author informatics
 */
public class Lab03test {
    
    public Lab03test() {
    }

    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
     @Test
    public void testCheckrowX1Win_output_true(){
        char[][] table = {{'X', 'X', 'X'}, {'-', '-', '-'}, {'-', '-', '-'}};
        boolean result =Lab03.checkRowWin(table);
        assertEquals(true, result); 
    }
    @Test
    public void testCheckrowX2Win_output_true(){
        char[][] table = {{'-', '-', '-'}, {'X', 'X', 'X'}, {'-', '-', '-'}};
        boolean result =Lab03.checkRowWin(table);
        assertEquals(true, result); 
    }
    @Test
    public void testCheckrowX3Win_output_true(){
        char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'X', 'X', 'X'}};
        boolean result =Lab03.checkRowWin(table);
        assertEquals(true, result);
    }
    @Test
     public void testCheckcolX1Win_output_true(){
        char[][] table = {{'X', '-', '-'}, {'X', '-', '-'}, {'X', '-', '-'}};
        boolean result =Lab03.checkColWin(table);
        assertEquals(true, result);
     }
    @Test
    public void testCheckcolX2Win_output_true(){
        char[][] table = {{'-', 'X', '-'}, {'-', 'X', '-'}, {'-', 'X', '-'}};
        boolean result =Lab03.checkColWin(table);
        assertEquals(true, result);
    }
    @Test
    public void testCheckcolX3Win_output_true(){
        char[][] table = {{'-', '-', 'X'}, {'-', '-', 'X'}, {'-', '-', 'X'}};
        boolean result =Lab03.checkColWin(table);
        assertEquals(true, result);
    }
    @Test
     public void testCheckdiagonalX1Win_output_true(){
        char[][] table = {{'X', '-', '-'}, {'-', 'X', '-'}, {'-', '-', 'X'}};
        boolean result =Lab03.checkDiagonalWin(table);
        assertEquals(true, result);
    }
    @Test
     public void testCheckdiagonalX2Win_output_true(){
        char[][] table = {{'-', '-', 'X'}, {'-', 'X', '-'}, {'X', '-', '-'}};
        boolean result =Lab03.checkDiagonalWin(table);
        assertEquals(true, result);
    }
    @Test
     public void testCheckdraw_output_true(){
        char[][] table = {{'O', 'X', 'O'}, {'O', 'X', 'X'}, {'X', 'O', 'X'}};
        boolean result =Lab03.BoardFull(table);
        assertEquals(true, result);
    }
}
